FROM python:3.9

COPY ./src /app/src
COPY ./requirements.txt /app
COPY ./alembic /app/alembic
COPY ./alembic.ini /app

WORKDIR /app

RUN pip3 install -r requirements.txt

EXPOSE 8080