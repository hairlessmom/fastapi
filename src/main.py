from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
from .database import get_db
from .schemas import CreateJobRequest
from .models import Job
import requests
from .dependencies import get_query_token, get_token_header
from .routers import items, users


app = FastAPI(dependencies=[Depends(get_query_token)])

app.include_router(users.router)
app.include_router(items.router)


@app.post("/")
def create(details: CreateJobRequest, db: Session = Depends(get_db)):
    to_create = Job(
        title=details.title,
        description=details.description
    )
    db.add(to_create)
    db.commit()
    return {
        "success": True,
        "created_id": to_create.id
    }


@app.get("/ping/")
def read_root():
    return requests.post(url="https://webhook.site/a9ebadb9-98dc-43f5-91e6-ae64285ed054", json={"message": "hello"},
                         verify=False)


@app.get("/")
def get_by_id(id: int, db: Session = Depends(get_db)):
    response = db.query(Job).filter(Job.id == id).first()
    if response is None:
        raise HTTPException(status_code=404, detail="Должность не найдена в базе")
    return response


@app.delete("/")
def delete(id: int, db: Session = Depends(get_db)):
    db.query(Job).filter(Job.id == id).delete()
    db.commit()
    return {"success": True}
